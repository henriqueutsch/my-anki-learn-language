100 palavras - 50%
300 - 70
1000 - 80
3k 5k - 85
7k - Vocabulário mínimo

There are roughly 100,000 word-families in the English language.
A native English speaking person knows between 10,000 (uneducated) to 20,000 (educated) word families.
Need to know 8,000-9,000 word families to enjoy reading a book.
A person with a vocabulary size of 2,500 passive word-families and 2,000 active word-families can speak a language fluently.

Objetivo ingles: Compreensão instantanea


- Basico:
- Intermediário:
https://www.youtube.com/watch?v=Z1RIR0J58mI

Front:
Try to use the revolving door.

Back:
Revolving [rɪˈvɒlvɪŋ]
(that turns) rotativo, giratório

- Avançado:
https://www.youtube.com/watch?v=BcNzOncA-UQ

Front:
A recent survey showed 75% of those questioned were in favour of the plan.

Back:
Survey  [ˈs3ːrveɪ]
an investigation of the opinions, behaviour, etc. of a particular group of people, which is usually done by asking them questions


Create Anki Decks Quickly Using CSV - With or Without Audio (2019)
https://www.youtube.com/watch?v=w_gieyVnKXs

- Phonics
Front:
The - Iphonics/-dze 
o, a, os, as - Imagine dizer os artigos "o"

Back:
[Palavraemingles] Iphonics //
[Tradução] EDNA

Dicionário fonético
https://unsidiomas.com.br/blog/dicionario-fonetico-saiba-pronunciar-palavras-em-ingles/v
https://www.superprof.com.br/blog/pronuncia-lingua-inglesa/


Fonte: The American Heritage Word Frequency Book by John B. Carroll, Peter Davles, and Barry Richman (Houghton Mlfflln, 1971, ISBN 0-395-13570-

AS 1000 PALAVRAS MAIS COMUNS EM INGLÊS QUE APARECEM EM MAIS DE 80 % DE QUALQUER CONVERSA (a pronuncia pode ser encontrada dentro das novelas sonoras no BOOK 2)
Segue abaixo o estudo que pesquisou a frequência das palavras inglêsas.
Em 1997 um estudo foi realizado para determinar as palavras mais comuns da língua inglêsa e seu percentual de ocorrência. Para este estudo os pesquisadores utilizaram os livros online do Projeto Gutemberg. Este projeto, integrado por voluntários, tem por objetivo digitalizar obras de literatura cujos direitos autorais tenham expirado. Nos Estados Unidos uma obra é colocada no domínio público 60 anos após a morte do autor. Obras de autores como Jane Austen, Conan Doyle, Edgar Rice Burroughs, e muitos outros estão disponíveis gratuitamente na Internet. 'Depo1;1;efie1;'te1W;!'01;),~'t'mao'tofionaepocafiape1;qu'ha,fJ'Zemo1;en'flion01;1;01c;Yt1CU)0~1s;.)~'t'm1ivroscom"binafiosgeraramumarquIvo
de 680 MB contendo aproximadamente sete milhões de palavras. Os resultados foram bastante surpreendentes.
As 250 palavras mais comuns compõem cerca de 60% de qualquer texto. Em outras palavras, se você conhece as 250 palavras mais comuns, 60% de qualquer texto em inglês é composto de palavras familiares.
Para facilitar ainda mais a tarefa os cognatos, que são as palavras parecidas em ambos os idiomas (possible e possível, por exemplo), totalizam entre 20 a 25% do total das palavras. Como o significado dos cognatos nos é de fácil compreensão, devido à semelhança com palavras de nosso idioma natal, temos então de 80 a 85% (60% + 20% =80%) do problema de vocabulário resolvido.
Se subirmos o número de palavras mais comuns a 1.000, chegamos a 70%. Somando a este valor oscognatos chegamos a valores entre 90 e 95% de um texto. Isto significa que para você aprender inglês rapidamente, vale a pena aprender a lista das palavras mais comuns. Neste manual incluímos a lista das 1000 palavras mais comuns. Além disso incluí também uma lista das 1.000 palavras mais comuns encontradas em qualquer conversa (não apenas em inglês). Se você estudar esta lista (ou uma delas), você vai ser capaz de entender 90 a 95% de qualquer conversa. Desde que todas essas palavras aparecem dentro do conteúdo das novelas sonoras, deixei-as na forma de Flash Cards com a pronúncia. Você pode fazer xerox dessas páginas e estudá-Ias, uma a uma, enquanto a música barroca está tocando no fundo.

100 palavras - 50%
250 - 60%
300 - 70%
1000 - 80%
3k 5k - 85%
7k - Vocabulário mínimo


ANKI PARA APRENDER INGLÊS (TUTORIAL COMPLETO)
https://www.youtube.com/watch?v=RTpGp2P0dkc&t=99s


#Word Frequency
https://www.wordfrequency.info/5k_lemmas_download.asp
https://www.talkenglish.com/vocabulary/top-2000-vocabulary.aspx
https://en.wiktionary.org/wiki/Wiktionary:Frequency_lists
https://invokeit.wordpress.com/frequency-word-lists/
https://github.com/hermitdave/FrequencyWords/blob/master/content/2018/en/en_50k.txt
http://www.naturalenglish.club/esl/

#ExtensionAnki

csv exporter
https://ankiweb.net/shared/info/1207511854

Remote Decks: Anki collaboration using Google Docs
https://ankiweb.net/shared/info/911568091

Zoom 2.1
https://ankiweb.net/shared/info/1846592880

ReviewHeatmap
https://ankiweb.net/shared/info/723520343

AwesomeTTS for Anki 2.1, updated
https://ankiweb.net/shared/info/427598962

#Dicionários
Iniciante
https://context.reverso.net/traducao/
https://www.linguee.com.br/portugues-ingles/

Intermediário
https://www.wordreference.com/

Avançado
https://www.thefreedictionary.com/
https://www.oxfordlearnersdictionaries.com/us/