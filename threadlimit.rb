10_000.times do |i|
  Thread.new { sleep }
rescue ThreadError
  puts "Your thread limit is #{i} threads"
  Kernel.exit(true)
end