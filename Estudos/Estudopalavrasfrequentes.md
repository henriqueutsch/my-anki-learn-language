https://www.wordfrequency.info/5k_lemmas_download.asp
https://www.talkenglish.com/vocabulary/top-2000-vocabulary.aspx
https://en.wiktionary.org/wiki/Wiktionary:Frequency_lists
https://invokeit.wordpress.com/frequency-word-lists/
https://github.com/hermitdave/FrequencyWords/blob/master/content/2018/en/en_50k.txt
http://www.naturalenglish.club/esl/
https://www.lextutor.ca/freq/lists_download/longman_3000_list.pdf
https://www.oxfordlearnersdictionaries.com/oxford_3000_profiler.html
https://1000mostcommonwords.com/

Nivel Solia

https://www.sprachcaffe.com/portugues/niveis-de-idiomas.htm




#Estudos
- Projeto Gutemberg [https://www.gutenberg.org/wiki/Main_Page]
https://pt.wikipedia.org/wiki/Projeto_Gutenberg
http://self.gutenberg.org/articles/eng/most_common_words_in_english


O Projecto Gutenberg foi iniciado por Michael Hart em 1971. Hart, um estudante na Universidade de Illinois, obteve acesso a um supercomputador Xerox Sigma V no Laboratório de Pesquisa de Materiais da universidade. Através de uns operadores simpáticos, recebeu uma conta com uma quantidade de tempo de computação virtualmente ilimitada; o seu valor na altura tem sido calculado variavelmente entre os 100.000 ou 100.000.000 dólares. [1] Hart disse que queria "retribuir" esta oferta fazendo algo que pudesse ser considerado de grande valor.

Este computador em particular era um de 15 nodos na rede de computadores que se tornaria a Internet. Hart acreditava que um dia os computadores estariam acessíveis ao público em geral e decidiu disponibilizar obras de literatura em formato electrónico de graça. Usou uma cópia da Declaração de Independência dos Estados Unidos que tinha na sua mochila e este foi o primeiro texto-e do Project Gutenberg. Deu o nome ao projeto em honra de Johannes Gutenberg, o impressor alemão do século XV que impulsionou a revolução da prensa móvel.

- Open Subtitles
https://github.com/hermitdave/FrequencyWords/

- Longman 3,000

The Longman Communication 3000 is a list of
the 3000 most frequent words in both spoken
and written English, based on statistical
analysis of the 390 million words contained
in the Longman Corpus Network – a group of
corpuses or databases of authentic English
language. The Longman Communication 3000
represents the core of the English language and
shows students of English which words are the
most important for them to learn and study
in order to communicate effectively in both
speech and writing.

- Corpus of Contemporary American English (COCA) (560 MI Words)

- iWeb [https://www.english-corpora.org/iweb/] (14 BI Words)
The iWeb corpus contains 14 billion words (about 25 times the size of COCA) in 22 million web pages. It is related to many other corpora of English that we have created, which offer unparalleled insight into variation in English.

Unlike other large corpora from the web, the nearly 95,000 websites in iWeb were chosen in a systematic way, and the websites have an average of 240 web pages and 145,000 words each. You can very easily and quickly focus on specific websites to create "virtual corpora" for any topic, such as buddhism, chocolate, basketball, or nuclear energy.


#Comparativos
https://www.english-corpora.org/iweb/help/iweb_overview.pdf
https://corpus.byu.edu/coca/compare-bnc.asp
https://www.english-corpora.org/


iWeb: The Intelligent Web-based Corpus	14 billion
News on the Web (NOW)	8.2 billion
Global Web-Based English (GloWbE)	1.9 billion
Wikipedia Corpus	1.9 billion
Hansard Corpus	1.6 billion
Early English Books Online	755 million
Corpus of Contemporary American English (COCA)	560 million
Corpus of Historical American English (COHA)	400 million
The TV Corpus   NEW  	325 million
The Movie Corpus   NEW  	200 million
Corpus of US Supreme Court Opinions	130 million
TIME Magazine Corpus	100 million
Corpus of American Soap Operas	100 million
British National Corpus (BNC) *	100 million
Strathy Corpus (Canada)	50 million
CORE Corpus	50 million
Longman 3000
Natural English Club 6000











AS 1000 PALAVRAS MAIS COMUNS EM INGLÊS QUE APARECEM EM MAIS DE 80 % DE QUALQUER CONVERSA

Segue abaixo o estudo que pesquisou a frequência das palavras inglêsas.
Em 1997 um estudo foi realizado para determinar as palavras mais comuns da língua inglêsa e seu percentual de ocorrência. Para este estudo os pesquisadores utilizaram os livros online do Projeto Gutemberg. Este projeto, integrado por voluntários, tem por objetivo digitalizar obras de literatura cujos direitos autorais tenham expirado. Nos Estados Unidos uma obra é colocada no domínio público 60 anos após a morte do autor. Obras de autores como Jane Austen, Conan Doyle, Edgar Rice Burroughs, e muitos outros estão disponíveis gratuitamente na Internet. De posse destes livros, 1600 ao todo na época da pesquisa, fizemos então nosso cálculos. Os 1600 livros combinados geraram um arquivo de 680 MB contendo aproximadamente sete milhões de palavras. Os resultados foram bastante surpreendentes.

As 250 palavras mais comuns compõem cerca de 60% de qualquer texto. Em outras palavras, se você conhece as 250 palavras mais comuns, 60% de qualquer texto em inglês é composto de palavras familiares.
Para facilitar ainda mais a tarefa os cognatos, que são as palavras parecidas em ambos os idiomas (possible e possível, por exemplo), totalizam entre 20 a 25% do total das palavras. Como o significado dos cognatos nos é de fácil compreensão, devido à semelhança com palavras de nosso idioma natal, temos então de 80 a 85% (60% + 20% =80%) do problema de vocabulário resolvido.
Se subirmos o número de palavras mais comuns a 1.000, chegamos a 70%. Somando a este valor oscognatos chegamos a valores entre 90 e 95% de um texto. Isto significa que para você aprender inglês rapidamente, vale a pena aprender a lista das palavras mais comuns. Neste manual incluímos a lista das 1000 palavras mais comuns. Além disso incluí também uma lista das 1.000 palavras mais comuns encontradas em qualquer conversa (não apenas em inglês). Se você estudar esta lista (ou uma delas), você vai ser capaz de entender 90 a 95% de qualquer conversa. Desde que todas essas palavras aparecem dentro do conteúdo das novelas sonoras, deixei-as na forma de Flash Cards com a pronúncia. Você pode fazer xerox dessas páginas e estudá-Ias, uma a uma, enquanto a música barroca está tocando no fundo.




https://www.fluentu.com/blog/how-many-words-do-i-need-to-know/
https://www.lextutor.ca/cover/papers/schmitt_etal_2011.pdf





https://www.sk.com.br/sk-laxll.html