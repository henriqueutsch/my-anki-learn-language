#O Método
Etapa 1 - A fundação             (2 meses)[500 Frases, 8 Textos]
Etapa 2 - Mineração de Sentenças (2 meses) [1000 Frases, 8 textos]
Etapa 3 - O Mar                  (3.5 meses) [2000 frases, 14 textos]
Etapa 4 - A transição Monolíngue (3.5 meses) [3000 frases, 15 textos]
Etapa 5 - Os estudos Monolíngues (13 meses) [Até 7000 frases]
Etapa 6 - A conversação          (Para sempre) [Fluencia]

- A regra de paretto

Aprender a técnica de aprender textos com audios - Anki
Disciplina
Não tem tempo para ficar em um módulo
Teoria do balde: De gota em gora vc vai aprendendo e parece que nunca vai encher. Um dia o balde estará cheio.
Só não pode deixar a gota secar
Tem que fluir, o fluxo

A principal habilidade de um professor é tolerar erros e corrigir com amor e carinho

Quanto tempo você demora para esquecer algo? Depende do nível de concentração e vontade de reter? Vontade é sentimento?

#Etapa 1 - Fundação Técnica texto com áudio
- Técnica estudos texto com áudio
1 - Texto em inglês
2 - Áudio desse texto

A Técnica
1 - Escutar o áudio do texto
2 - Assistir a aula
3 - Estudar o texto linha por linha
4 - Ler e escutar o texto/audio ao mesmo tempo (5 a 10 vezes)
5 - Escutar somente o áudio sem texto (5 a 10 vezes)

Resultado esperado: uma melhora no entendimento do texto

O Anki - Sistema de gestão de repetição espaçada
Curva do esquecimento e revisões
1 - Inserir as informações que vc quer aprender
2 - Estudar/Revisar todos os dias

Frases = Vocabulário + Gramática

O Anki não é "colocou no Anki, tá aprendido"
O método não é só o Anki

-Instalação
1 - instalar Anki
2 - Cadastrar Anki Web para sincronizar
3 - Renomear o título
4 - Renomear baralho
5 - instalar extenções
    - Zoom 2.1
6 - Configurar opções do deck






