# https://translate.google.com/#view=home&op=translate&sl=en&tl=pt&text=start
# http://watir.com/guides/chrome/#headless
require 'open-uri'
require 'nokogiri'
require 'watir'
require 'headless'

def gethtml(word)
        browser = Watir::Browser.new :chrome, headless: true
        browser.goto "https://translate.google.com/#view=home&op=translate&sl=en&tl=pt&text=#{word}"
        return browser.html
end

wordlist = Array.new
scorelist = Array.new
filter = Array.new
document = Nokogiri::HTML.parse(gethtml("your"))
document.search('span.gt-baf-word-clickable').each{|word|
    # puts "\n#{word.text} |"
    wordlist << word.text
}
document.search('div.gt-baf-entry-score').each{|score|
    # puts score.search('div.filled').count
    scorelist << score.search('div.filled').count
}
p wordlist
p wordlist.count
p scorelist
p scorelist.count

# p scorelist.each_index.select {|score,v| score > 2}
# p scorelist.index {|score,v| score > 2}
scorelist.each_with_index{|word,index| filter << wordlist[index] if word > 2 }
puts filter.join(",")
