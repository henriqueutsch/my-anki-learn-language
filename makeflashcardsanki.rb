# https://github.com/c2h2/tts
# https://interlingua.fandom.com/wiki/Frequency_lists
# https://en.wiktionary.org/wiki/Wiktionary:Frequency_lists/PG/2006/04/1-10000
# https://en.wiktionary.org/wiki/Wiktionary:Frequency_lists/PG/2006/04/10001-20000
# https://github.com/jseidl/Multi-TOR
# https://olivierlacan.com/posts/concurrency-in-ruby-3-with-guilds/
# https://docs.ruby-lang.org/en/3.0.0/Ractor.html
# https://medium.com/@andresakata/background-job-processing-using-ractor-ruby-3-41c7956d14a0
# https://www.fastruby.io/blog/ruby/performance/how-fast-are-ractors.html
# https://github.com/ruby/ruby/blob/master/doc/ractor.md#worker-pool
# https://www.speedshop.co/2020/05/11/the-ruby-gvl-and-scaling.html
require 'open-uri'
require 'net/http'
require 'net/https'
require 'tts'
require 'nokogiri'
require 'watir'
require 'headless'
require 'selenium-webdriver'
require 'celluloid/current'

class Request
    include Celluloid
    include Celluloid::Notifications
    INACTIVITY_TIMEOUT = 900

    def initialize
        subscribe 'Done => GetHTML', :gethtml
        subscribe 'Done => GetWord', :getword
    end

    def gethtml(word)
        browser = Watir::Browser.new :chrome, headless: true
        browser.goto "https://translate.google.com/#view=home&op=translate&sl=en&tl=pt&text=#{word}"
        while browser.ready_state.eql? "complete"
            p "Status Request> #{browser.ready_state}"
            browser.wait
            if browser.ready_state.eql? "complete"
                response = browser.html
                browser.close
                return response
            end
        end
    end

    def getword(word)
        begin
            parsed_url = URI.parse("https://www.wordreference.com/enpt/#{word}")
            # http = Net::HTTP.new(parsed_url.host, parsed_url.port)
            http = Net::HTTP.start(parsed_url.host, parsed_url.port, use_ssl: true, verify_mode: OpenSSL::SSL::VERIFY_NONE)
            # http.use_ssl = true
            # http.verify_mode = OpenSSL::SSL::VERIFY_NONE
            document = Nokogiri::HTML.parse(URI.open(parsed_url))
            if document.at_css("span#pronWR").nil?
                return 'none'
            else
                return document.at_css("span#pronWR").text
            end
        rescue StandardError => e
            p "Rescued GetWord: #{e.inspect}"
            p "Rescued GetWord 2: #{e}"
        end
    end

    def getdic(word)
        begin
            # p "#{word} BEGIN ------------------"
            wordlist = Array.new
            scorelist = Array.new
            filter = Array.new
            # p "#{word} BEGIN CON------------------"
            document = Nokogiri::HTML.parse(gethtml(word))
            # p "#{word} END CON ------------------"
            p "Search number: #{document.search('span.gt-baf-word-clickable').count}"
            document.search('span.gt-baf-word-clickable').each{|word|
                p"\n#{word.text} |"
                wordlist << word.text
            }
            p "Search number: #{document.search('div.gt-baf-entry-score').count}"
            document.search('div.gt-baf-entry-score').each{|score|
                # puts score.search('div.filled').count
                scorelist << score.search('div.filled').count
            }
            # p wordlist
            # p wordlist.count
            # p scorelist
            # p scorelist.count
            scorelist.each_with_index{|word,index| filter << wordlist[index] if word > 1 }
            # p "#{word} END ------------------"
            return filter.join(",")
        rescue StandardError => e
            puts "Rescued getdic: #{e.inspect}"
        end

    end

end

def ThreadLimit
    10_000.times do |i|
        a = Thread.new { sleep }
        Thread.kill(a)
      rescue ThreadError
        puts "Your thread limit is #{i} threads"
        return i
        Kernel.exit(true)
      end
end

Dir["#{Dir.pwd}/audios/*.mp3"].map {|file| File.delete(file)}

linenum = 1
mylist = Array.new
p "Size of pools: #{ThreadLimit()}"
card_pool = Request.pool(size: 20)
File.open("./BaseWords/en_50k.txt").take(10).each { 
    |word| word = word.delete "\s\n1234567890"
        # translate = "#{linenum} => #{word} - Iphonics / #{getword(word)} / Tradução: | #{getdic(word)} |"
        p translate = "#{linenum} => #{word} - Iphonics / #{card_pool.future.getword(word)} / Tradução: | #{card_pool.future.getdic(word)} |"
        mylist << translate
        word.to_file "en", "audios/#{word}.mp3"
        linenum += 1
}
# pool.terminate
puts '-----------------------------------'
puts mylist
# Front
# The - Iphonics/-dze 
# o, a, os, as - Imagine dizer os artigos "o"

# Back
# [Palavraemingles] Iphonics //
# [Tradução] EDNA
